# Watch Station International

# Markdown Styles

## How to Order
<span class="description">Ordering on our site is designed to be a quick and easy process. Just follow these simple steps.</span>

#### Finding the Product
By using the menus on the left-hand side of the page, you can browse our site by gender and type of product. You can also search for a specific item using the _search_ box located in the upper right-hand corner of the screen. To access detailed information about a product, click on the image of the item you are interested in and you will be taken to the item's detail page.

#### Using the Shopping Cart
To place a product in your shopping cart from the product page, select the desired size, color, and quantity of the item you wish to purchase, and click _Add to Cart_. Select the _Continue Shopping_ button to continue browsing, or the _Checkout_ button to complete your order.

While in your shopping cart, you can remove the item at any time. Simply click the _Remove_ button.

Remember, by clicking the link in the upper right-hand corner of the screen, you may view your shopping cart at any time. This allows you to add, change, or delete items whenever you like.

#### Checking Out
When you're ready to complete your order, be sure to review all of the items you've placed in your shopping cart. To check out, follow the instructions on the screen to finalize your order. You may check out without signing in, creating an account, or using an existing account.

#### Shipping and Billing
Once you've logged in, you'll need to provide your shipping and/or billing information (note: billing address must be entered exactly as it appears on your credit card statement), choose a shipping method, and select your method of payment. We accept __Visa, MasterCard, American Express, Discover Card, and watchstation.com eGift Card__. If you're paying with a credit card, enter the number without spaces or dashes.

#### Completing Your Order
Be sure to review all of the information that you have provided, and make any necessary changes before clicking the _Place Order Now_ button. Once you complete your order, you will see a confirmation page with your confirmation number. Additionally, we will send you emails with your Order Confirmation information and when your order is shipped. Please note that as a result of our speedy order processing, we are unable to change or cancel an order once it has been finalized. Please make sure your order is accurate before submitting it for processing. Also, please allow up to 1-2 business days for processing.

#### Helpful Information
If you cannot find the product you are looking for, or if you would prefer to phone in your order, please __contact our Customer Care department at 855.21.WATCH (855.219.2824)__ and we will be happy to assist you. Hours are M-F, 7am - 8pm CST and Sat., 9:30am - 6pm. Your satisfaction is 100% guaranteed. All items in your order can be returned to Watchstation 90 days from the purchase date, for a complete refund if you are unsatisfied. The returned product must be in new condition. Detailed return instructions will be included with your order.


* * *


## Shipping Policy
`Our site ships Monday through Friday excluding holidays. All shipping dates are pending credit card verification and in-stock merchandise.
__Please note: orders over $500 require a signature upon delivery.__`

#### Shipping Time and Availability

<table class="shippingTable" border="0" cellspacing="0">
	<tbody>
		<tr>
			<th>STANDARD</th>
			<th>NEXT DAY</th>
		</tr>
		<tr class="a">
			<td>Arrives in 3-5 business days**</td>
			<td>Arrives next business day**</td>
		</tr>
		<tr class="b">
			<td>Free Standard on <strong>all orders</strong>.</td>
			<td>All orders-<strong>$19.95</strong></td>
		</tr>
		<tr class="a">
			<td>Standard delivery is available for orders with delivery addresses within the United States and territories.(excluding APO/FPO, and PO Boxes).</td>
			<td>Next day delivery is only available for orders with delivery addresses within the United States (excluding APO/FPO, PO Boxes, and Rural Routes).</td>
		</tr>
	</tbody>
</table>

We may need to divide your order into two or more shipments. You will receive a separate shipping confirmation email for each shipment we send to you. Your shipping charges will not be affected by the number of shipments we divide your order into.

* Watch Station may change shipping rates at any time.
** The shipping cut-off time is 12pm (CDT). Orders placed after 12pm (CDT) will be processed the next business day.


* * *


# Shop with Confidence
### FREE SHIPPING
Enjoy free shipping with every order.

### FREE RETURNS
We accept returns for up to 90 days from purchase, and the shipping's on us. We include a prepaid UPS return label with every package.

### A PERFECTLY SIZED WATCH, EVERY TIME
Use our printable Wrist Sizer tool to measure your wrist. Enter your measurements when you place your order, and we will pre-size your timepiece to make sure it arrives set to the correct number of links for you.
