var md = require("node-markdown").Markdown
	, express = require('express')
	, app = express()
	, fs = require('fs');

app.use(express.logger('dev'));

app.use(express.bodyParser());

//app.use(express.static(__dirname));

var header = fs.readFileSync(__dirname+"/header.html", 'utf8', function (err,data) {
	if (err)
		console.log( 'you should add a header.html file to your site' );
	else
		return data;
});

var footer = fs.readFileSync(__dirname+"/footer.html", 'utf8', function (err,data) {
	if (err)
		console.log( 'you should add a footer.html file to your site' );
	else
		 return data;
});

app.use(function(req, res){
	//console.log(req._parsedUrl.pathname);

	if (req._parsedUrl.pathname == '/favicon.ico') { res.send('no'); return; }

	console.log(header);

	fs.readFile(__dirname+req._parsedUrl.pathname, 'utf8', function (err,data) {
		if (err)
			res.send( 'sorry, '+req._parsedUrl.pathname+' is not a file' );
		else
			res.send( header + md(data) + footer );
	});

});

app.listen(8886);
console.log('Listening on port 8886');